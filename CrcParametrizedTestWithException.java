//import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

@RunWith(value = Parameterized.class)
public class CrcParametrizedTestWithException {

	public CrcParametrizedTestWithException(long data, long poly)
	{
		m_data = data;
		m_poly = poly;
	}
		
	private long m_data;
	private long m_poly;
	
	@Parameters
	public static Collection<Object[]> data() 
	{
		return Arrays.asList(new Object[][] {
			{ 0x0, 0x0 }
		});
	}
	
	@Rule
	public final ExpectedException thrown = ExpectedException.none();
	
	@Test()
	public void test() throws FirstBitNotSetException {
		Crc crc = new Crc(m_data, m_poly);
		thrown.expect(FirstBitNotSetException.class);
		crc.calculate();
	}
}

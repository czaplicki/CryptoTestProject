import static org.junit.Assert.*;

import org.junit.Test;

public class GcdTest {

	@Test
	public void test() {
		Gcd gcd = new Gcd(21,27);
		
		assertEquals(3, gcd.calculate());		
	}
	
	@Test
	public void test2() {
		Gcd gcd = new Gcd(9,10);
		
		assertEquals(1, gcd.calculate());
	}
	
	@Test
	public void test3() {
		Gcd gcd = new Gcd(10,9);
		
		assertEquals(1, gcd.calculate());
	}
	
	@Test
	public void test4() {
		Gcd gcd = new Gcd(100,25);
		
		assertEquals(25, gcd.calculate());
	}
	
	@Test
	public void test5() {
		Gcd gcd = new Gcd(973,301);
		
		assertEquals(7, gcd.calculate());
	}
	
	@Test
	public void test6() {
		Gcd gcd = new Gcd(972,301);
		
		assertEquals(1, gcd.calculate());
	}
	
	@Test
	public void test7() {
		Gcd gcd = new Gcd(0,0);
		
		assertEquals(0, gcd.calculate());
	}
	
	@Test
	public void test8() {
		Gcd gcd = new Gcd(1,0);
		
		assertEquals(1, gcd.calculate());
	}
	
	@Test
	public void test9() {
		Gcd gcd = new Gcd(0,99);
		
		assertEquals(99, gcd.calculate());
	}
}

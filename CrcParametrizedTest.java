import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import org.junit.Test;

@RunWith(value = Parameterized.class)
public class CrcParametrizedTest {

	public CrcParametrizedTest(long data, long poly, long crc)
	{
		m_data = data;
		m_poly = poly;
		m_crc = crc;
	}
		
	private long m_data;
	private long m_poly;
	private long m_crc;
	
	@Parameters
	public static Collection<Object[]> data() 
	{
		return Arrays.asList(new Object[][] {
			{ 0x12, 0x11D, 0xF7},
			{ 0x12, 0x7, 0x0 },
			{ 0x65, 0xD, 0x0 }
		});
	}
	
	@Test
	public void test() throws Exception {
		Crc c = new Crc(m_data, m_poly);
		assertEquals(m_crc, c.calculate());
	}
	
}

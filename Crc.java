
public class Crc {
	public Crc(long data, long poly)
	{
		m_data = data;
		m_poly = poly;
	}
	
	public long calculate() throws FirstBitNotSetException
	{
		int fbsData, fbsPoly;
		fbsPoly = firstSetBit(m_poly);
		if (fbsPoly == -1 || firstSetBit(m_data) == -1)
		{
			throw new FirstBitNotSetException("no first bit set");
		}
		
		long rem = m_data << fbsPoly;
		
		while(true)
		{
			fbsData = firstSetBit(rem);
			
			int diff = fbsData - fbsPoly; 
			if (diff < 0)
				return rem;
		
			long sPoly = m_poly << diff;
		
			rem = rem ^ sPoly;
		}
	}
	
	private int firstSetBit(long p_in)
	{
		int index = 64 - Long.numberOfLeadingZeros(p_in) - 1;
		if (index < 0)
		{
			return -1;
		}
		return index;
	}
	
	private long m_data;
	private long m_poly;
}

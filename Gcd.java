
public class Gcd {
	Gcd(long a, long b)
	{
		m_a = a;
		m_b = b;
	}
	
	public long calculate()
	{
		return calculateImpl(m_a, m_b);
	}
	
	private long calculateImpl(long a, long b)
	{
		if (a == 0)
			return b;
		if (b == 0)
			return a;
		if (a == 0 && b == 0)
			return 0;
		if (a < b )
			return calculateImpl(b, a);
		long r = a % b;
		if (r == 0)
			return b;
		else
			return calculateImpl(b, r);
	}
	
	private long m_a;
	private long m_b;
}
